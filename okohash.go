package okohash

import (
	"fmt"

	"gitlab.com/ashinnv/okolog"
	"golang.org/x/crypto/bcrypt"
)

func HashAndSalt(input string) string {

	fmt.Println(input)
	pwd := []byte(input)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		okolog.ErrorSendoff("localhost:8081", "genericMessage: ", err)
	}

	return string(hash)
}

func ComparePasswords(hashedPwd string, plainPwd []byte) bool {

	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		okolog.ErrorSendoff("localhost:8081", "compare passwords failure: ", err)
		return false
	}

	return true
}
